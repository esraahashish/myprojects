package eg.edu.alexu.csd.oop.game.state;

import java.awt.Color;
import java.awt.FileDialog;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

public class Display implements Runnable{
	
	private static final Logger logger = Logger.getLogger(Display.class);
	private String LevelDifficulty ;
	private String path = null ;

	public JFrame frame;
	private JButton option, exit;
	private JLabel label;
	private String[] difficulty = { "Easy", "Medium", "Hard" };

	@Override
	public void run() {
		frame = new JFrame("Candy Crush");
		frame.setBounds(0, 0, 900, 680);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);

		exit = new JButton("Exit");
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		exit.setForeground(new Color(128, 0, 128));
		exit.setFont(new Font("Buxton Sketch", Font.BOLD, 20));
		exit.setBackground(new Color(255, 255, 224));
		exit.setBounds(350, 475, 200, 50);
		frame.getContentPane().add(exit);

		for (int i = 0; i < 3; i++) {
			addAction(i);
		}

		label = new JLabel("");
		Image img = new ImageIcon(this.getClass().getResource("/candy.jpg")).getImage();
		label.setIcon(new ImageIcon(img));
		label.setBounds(0, 0, 900, 680);
		frame.getContentPane().add(label);
		frame.setVisible(true);	
	}
	

	private void addAction(final int i) {
		option = new JButton(difficulty[i]);
		option.setForeground(new Color(128, 0, 128));
		option.setFont(new Font("Buxton Sketch", Font.BOLD, 20));
		option.setBounds(350, 250 + (i * 75), 200, 50);
		option.setBackground(new Color(255, 255, 224));
		option.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				LevelDifficulty = difficulty[i];
				logger.info("Display >> User has choosed to play " + LevelDifficulty + " game");				
				FileDialog dialog = new FileDialog(frame,"Choose Scene", FileDialog.LOAD);
				JOptionPane.showMessageDialog(null, "Choose Scene");
				dialog.setVisible(true);
				path = dialog.getDirectory() + dialog.getFile();
				logger.info("Display >> User selected the path Successfully  : " + path);
			}
		});
		frame.add(option);
	}

	public String getLevelDifficulty() {
		return LevelDifficulty;
	}
	
	public void disposeFrame(){
		frame.dispose();
	}
	
	public String getThemePath(){
		return this.path;
	}
}
