package eg.edu.alexu.csd.oop.game.state;

import javax.swing.JFrame;

import eg.edu.alexu.csd.oop.game.GameEngine;
import eg.edu.alexu.csd.oop.game.factory.Factory;


public class Game implements State {

	private View view;

	public Game(View view) {
		this.view = view;
	}

	@Override
	public void execute() {
		GameEngine.start("Candy Crush", new Factory().getStateObject(view.getClassName(),view.getThemePath()),JFrame.EXIT_ON_CLOSE);
	}

}
