package eg.edu.alexu.csd.oop.game.observer;

import eg.edu.alexu.csd.oop.game.GameObject;

public interface Observer extends GameObject{
   void update();
}
