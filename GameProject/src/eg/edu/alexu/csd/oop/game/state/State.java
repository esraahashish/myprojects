package eg.edu.alexu.csd.oop.game.state;

public interface State {

	public void execute();
}
