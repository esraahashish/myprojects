package eg.edu.alexu.csd.oop.game.dynamiclinkage;
import java.util.Map;

import eg.edu.alexu.csd.oop.game.World;


public abstract class ThemeLoadableClass {
	protected World world ;
	protected Map<String,Class<?>> map;
	public abstract void setEnvironment(World world);
	public abstract void start() ;
	public abstract void setClasses(Map<String,Class<?>> map) ;
	public abstract int getClownsNumber ();
}
