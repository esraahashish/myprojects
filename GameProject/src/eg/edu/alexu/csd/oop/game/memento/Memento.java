package eg.edu.alexu.csd.oop.game.memento;

import eg.edu.alexu.csd.oop.game.GameObject;

public class Memento {
		
		private GameObject gameObject;
		
		public Memento(GameObject gameObject) {
			this.gameObject = gameObject ;
		}
		
		protected GameObject get(){
			return this.gameObject;
		}

}
