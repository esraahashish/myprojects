package eg.edu.alexu.csd.oop.game.memento;

import eg.edu.alexu.csd.oop.game.GameObject;

public class Originator {
	

	public  Memento save(GameObject gameObject){
		return new Memento(gameObject) ; 
	}

}
