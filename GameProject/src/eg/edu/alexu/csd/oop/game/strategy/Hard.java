package eg.edu.alexu.csd.oop.game.strategy;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import eg.edu.alexu.csd.oop.game.GameObject;
import eg.edu.alexu.csd.oop.game.iterator.IteratorImpl;
import eg.edu.alexu.csd.oop.game.objects.ClownAttributes;
import eg.edu.alexu.csd.oop.game.observer.BombShape;
import eg.edu.alexu.csd.oop.game.observer.ObservableImpl;
import eg.edu.alexu.csd.oop.game.observer.Observer;
import eg.edu.alexu.csd.oop.game.snapshot.CareTaker;
import eg.edu.alexu.csd.oop.game.snapshot.Memento;
import eg.edu.alexu.csd.oop.game.snapshot.Originator;

public class Hard extends WorldImpl{
    private CareTaker  careTaker;
    private Originator originator ;
	private ObservableImpl observer ;
	private boolean Bomb = false ;
	private boolean saveCurrent = false;
	private boolean loadCurrent = false;

	public Hard(String path) {
		super() ;
		run(path);
		setBarShapes();
		this.bar1 = constant.get(0) ;
		this.bar2 = constant.get(1) ;
		this.observer = new ObservableImpl() ;
		this.careTaker = new CareTaker();
		this.originator = new Originator();
		for(int i = 0;i< 10;i++){
			GameObject object = new BombShape() ;
			observer.addObserver((Observer) object);
			this.moving.add(object) ;
		}
		observer.notifyAllObservers();
		super.targetScore = 25 ;
	}
	public boolean isBomb(GameObject object){
		return object.getClass().getSimpleName().equals("BombShape") ;
	}
	
	private void goToPrevState() {
//		int clownPos = 0;
//		int shapePos = 0;
		control.clear();
		LinkedList<Memento> memento = careTaker.load();
		for (Memento object : memento) {
			GameObject gameObject = object.get();
			if (gameObject instanceof ClownAttributes) {
				ClownAttributes obj = (ClownAttributes) gameObject;
				obj.setRightStackYPosition(object.getRight());
				obj.setLeftStackYPosition(object.getLeft());
				this.score = object.getScore() ;
			}
			control.add(gameObject);
		}
	}

	private void saveCurrentState() {
		for (GameObject object : control) {
			if (object instanceof ClownAttributes) {
				ClownAttributes obj = (ClownAttributes) object;
				Memento memento = originator.save(object);
				memento.setRight(obj.getRightStackYPosition());
				memento.setLeft(obj.getLeftStackYPosition());
				memento.setX(obj.getX());
				memento.setScore(this.score);
				careTaker.save(memento);

			} else {
				careTaker.save(originator.save(object));
			}
		}
	}
	@Override
	public boolean refresh() {
		long now = System.currentTimeMillis();
		boolean clownReachedBar = true;
		List<GameObject> moveableToControl = new LinkedList<GameObject>();
		List<GameObject> removedMovingShapes = new LinkedList<GameObject>();
		iterator = new IteratorImpl(moving);
		while (iterator.hasNext()) {
			GameObject object = iterator.next();
			if(!object.isVisible())continue ;
			GameObject bar = IntersectBar(object);
			if (bar != null && !isBomb(object)) {
				if (isLeft(bar)) {
					object.setX(object.getX() + 1);
					if (IntersectBar(object) == null) {
						moving.add(pool.getShape(randomShapeGenerator[new Random().nextInt(4)]));
					}
				} else {
					object.setX(object.getX() - 1);
					if (IntersectBar(object) == null) {
						moving.add(pool.getShape(randomShapeGenerator[new Random().nextInt(4)]));
					}
				}
			} else {
				ClownAttributes clown = intersectClown(object);
			if(isBomb(object)){
				if(clown != null && !loadCurrent){
					loadCurrent = true;
					this.goToPrevState();
				}
				//object.setX(object.getX()+1);
				object.setY(object.getY()+1);
			}else if (clown == null) {
					object.setY(object.getY() + 4);
					if (object.getY() > HEIGHT) {
						super.pool.releaseShape(object);
						removedMovingShapes.add(object);
					}
				} else {
					if (getClownLeftHand(clown).intersects(getShape(object))) {
						clown.getCareTaker().saveLeft(clown.getOriginator().save(object));
						LinkedList<GameObject> list = clown.getCareTaker().checkScore("left");
						if (list.size() != 0) {
							super.score++;
							clown.setLeftStackYPosition(clown.getLeftStackYPosition() + 2 * object.getHeight());
							for (GameObject dummy : list) {
								if (dummy != object) {
									//super.pool.releaseShape(dummy);
									super.control.remove(dummy);
								}
							}
							super.pool.releaseShape(object);
							removedMovingShapes.add(object);
						} else {
							clown.setLeftStackYPosition(clown.getLeftStackYPosition() - object.getHeight());
							moveableToControl.add(object);
						}
					} else if (getClownRightHand(clown).intersects(getShape(object))) {
						clown.getCareTaker().saveRight(clown.getOriginator().save(object));
						LinkedList<GameObject> list = clown.getCareTaker().checkScore("right");
						if (list.size() != 0) {
							super.score++;
							clown.setRightStackYPosition(clown.getRightStackYPosition() + 2 * object.getHeight());
							for (GameObject dummy : list) {
								if (dummy != object) {
									//super.pool.releaseShape(dummy);
									super.control.remove(dummy);
								}
							}
							super.pool.releaseShape(object);
							removedMovingShapes.add(object);
						} else {
							clown.setRightStackYPosition(clown.getRightStackYPosition() - object.getHeight());
							moveableToControl.add(object);
						}
					}
				}
			}
		}
		for (GameObject object : moveableToControl) {
			super.control.add(object);
			super.moving.remove(object);
		}
		for (GameObject object : removedMovingShapes) {
			moving.remove(object);
		}
		for (int i = 0; i < super.clownsNumber; i++) {
			clownReachedBar = clownReachedBar && super.reachBar(super.control.get(i));
		}
	
		if(now - super.startTime >= MAX_TIME/6 && !saveCurrent){
			this.saveCurrent = true ;
			this.saveCurrentState();
		} 
		
		if(now - super.startTime >= MAX_TIME/4 && !Bomb){
			observer.notifyAllObservers();
			Bomb = true ;
		}
		return (now - super.startTime < MAX_TIME) && !(clownReachedBar);
	}

	@Override
	public int getSpeed() {
		return 15;
	}

	@Override
	public int getControlSpeed() {
		return 25;
	}

	@Override
	public String getStatus() {
		return "Score = " + this.score + "   | |   Target Score = " + targetScore + "   | |   Timer = "
				+ (System.currentTimeMillis() - startTime) / 1000 + " From " + MAX_TIME / 1000;
	}


}
