package eg.edu.alexu.csd.oop.game.state;

import org.apache.log4j.Logger;

public class Intro implements State {

	private static final Logger logger = Logger.getLogger(Intro.class);
	
	private View view = new View();

	public Intro(View view) {
		this.view = view;
	}

	@Override
	public void execute() {
		Display display = new Display();
		Thread thread = new Thread(display);
		thread.start();
		try {
			do {
				Thread.currentThread();
				Thread.sleep(50);
			} while (display.getThemePath() == null);
			thread.join();
		} catch (InterruptedException e) {
			logger.fatal("Intro >> Thread interruption error Occured\n" + e);
		}
		display.disposeFrame();
		view.setClassName(display.getLevelDifficulty());
		view.setThemePath(display.getThemePath());
		logger.info("Intro >> Game State started Successfully");
		try{
			view.setState(new Game(view));
			view.start();
		}catch(RuntimeException e){
			logger.fatal("Intro >> Game Object can't be created" + e);
		}
	}

}
