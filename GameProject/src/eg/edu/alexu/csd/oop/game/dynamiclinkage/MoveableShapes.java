package eg.edu.alexu.csd.oop.game.dynamiclinkage;
import java.awt.image.BufferedImage;

import eg.edu.alexu.csd.oop.game.observer.Observer;


public class MoveableShapes implements Observer {
	// an array of sprite images that are drawn sequentially
	protected BufferedImage[] spriteImages = new BufferedImage[1];
	private int x;
	private int y;
	//private int randomIndex;
	protected boolean visible = true ;

	@Override
	public int getX() {
		return x;
	}

	@Override
	public void setX(int mX) {
		this.x = mX;
	}

	@Override
	public int getY() {
		return y;
	}

	@Override
	public void setY(int mY) {
		this.y = mY;
	}

	@Override
	public BufferedImage[] getSpriteImages() {
		return spriteImages;
	}

	@Override
	public int getWidth(){
		return spriteImages[0].getWidth();
	}
 
	@Override
	public int getHeight() {
		return spriteImages[0].getHeight();
	}

	@Override
	public boolean isVisible() {
		return visible;
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		this.visible = !visible ;
	}
	
	
}

