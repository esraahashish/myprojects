package eg.edu.alexu.csd.oop.game.snapshot;

import eg.edu.alexu.csd.oop.game.GameObject;

public class Memento {

		private GameObject gameObject  ;
		private int xPos ;
		private int score ;
		private int rightStack ;
		private int leftStack ;
		
		public Memento(GameObject gameObject) {
			this.gameObject = gameObject ;
		}

		public int getXpos(){
			return this.xPos;
		}
		
		public void setLeft(int left){
			 this.leftStack = left;
		}
		
		public int getScore(){
			return this.score;
		}
		public void setScore(int score){
			this.score = score;
		}
		
		public int getLeft(){
			return this.leftStack;
		}
		public int getRight(){
			return this.rightStack;
		}
		
		public void setRight(int right){
			 this.rightStack = right;
		}
		
		public GameObject get(){
			return this.gameObject;
		}

		public void setX(int x) {
           this.xPos = x;			
		}

}
