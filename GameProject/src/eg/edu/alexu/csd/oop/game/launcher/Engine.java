package eg.edu.alexu.csd.oop.game.launcher;

import org.apache.log4j.Logger;

import eg.edu.alexu.csd.oop.game.state.Intro;
import eg.edu.alexu.csd.oop.game.state.View;

public class Engine {
	private static final Logger logger = Logger.getLogger(Engine.class);

	public static void main(String[] args) {

		try{
			View view = new View();
			view.setState(new Intro(view));
			logger.info("Engine >> Intro State started Successfully");
			view.start();
		}catch(RuntimeException e){
			logger.fatal("Engine >> Intro Object can't be created" + e);
		}
	}
}
