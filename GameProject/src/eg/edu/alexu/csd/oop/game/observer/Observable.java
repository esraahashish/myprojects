package eg.edu.alexu.csd.oop.game.observer;

public interface Observable {
	void addObserver(Observer observer);
	void notifyAllObservers();
}
