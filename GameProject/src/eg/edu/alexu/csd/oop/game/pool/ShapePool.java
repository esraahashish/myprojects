package eg.edu.alexu.csd.oop.game.pool;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import eg.edu.alexu.csd.oop.game.GameObject;
import eg.edu.alexu.csd.oop.game.strategy.WorldImpl;

public class ShapePool {
	
	private  Map<String,Class<?>> classes;
	private int Height = WorldImpl.HEIGHT / WorldImpl.FACTOR;
	private int[] xPosition = { -32     , WorldImpl.WIDTH};
	private int[] yPosition = { Height, Height         };
	private static ShapePool shapePool = new ShapePool();
	private Map<String, LinkedList<GameObject>> map = new LinkedHashMap<String, LinkedList<GameObject>>();
	private int rateOfCreation = 0;
	
	private ShapePool(){
		this.map.put("BlueShape", new LinkedList<GameObject>());
		this.map.put("GreenShape", new LinkedList<GameObject>());
		this.map.put("RedShape", new LinkedList<GameObject>());
		this.map.put("YellowShape", new LinkedList<GameObject>());
	}
	
	
	public GameObject getShape(String objectType){
		this.rateOfCreation++;
		LinkedList<GameObject> objectList = map.get(objectType);
		if (objectList.size() == 0){
			try {
				GameObject object = (GameObject) classes.get(objectType).newInstance();
				this.intialize(object);
				return object ;
			} catch (InstantiationException | IllegalAccessException e) {
				throw new RuntimeException();
			}
		}else {
			GameObject object = objectList.getFirst();
			objectList.remove(object);
			this.intialize(object);
			return object;
		}
	}
	
	public void releaseShape(GameObject object) {
		String className = object.getClass().getSimpleName();
		map.get(className).add(object);
		return;
	}
	
	private void intialize(GameObject object){
		object.setX(xPosition[rateOfCreation % 2]);
		object.setY(yPosition[rateOfCreation % 2]-object.getHeight());
		return;
	}
	
	public static ShapePool getInstance() {
		return shapePool;
	}
	public void setClasses ( Map<String,Class<?>> map){
		classes = map;
	}
}
