package eg.edu.alexu.csd.oop.game.observer;

import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;

import eg.edu.alexu.csd.oop.game.dynamiclinkage.MoveableShapes;
import eg.edu.alexu.csd.oop.game.strategy.WorldImpl;

public class BombShape extends MoveableShapes implements Observer{
	
	private final int MAX_SIZE = 4;
	private String[] images = new String[MAX_SIZE];//load image manually
	public BombShape() {
		this.setX(new Random().nextInt(WorldImpl.WIDTH));
		this.setY(0);
		try {
			images[0] = "/purple.png";
			images[1] = "/purple.png";
			images[2] = "/purple.png";
			images[3] = "/purple.png";
			this.spriteImages[0] = ImageIO.read(getClass().getResourceAsStream(images[new Random().nextInt(4)]));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public void update() {
		this.visible = !visible ;
	}

}
