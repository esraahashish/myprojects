package eg.edu.alexu.csd.oop.game.strategy;
import java.awt.Rectangle;
import java.io.File;
import java.io.FileInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

import eg.edu.alexu.csd.oop.game.GameObject;
import eg.edu.alexu.csd.oop.game.World;
import eg.edu.alexu.csd.oop.game.dynamiclinkage.ThemeLoadableClass;
import eg.edu.alexu.csd.oop.game.iterator.Iterator;
import eg.edu.alexu.csd.oop.game.iterator.IteratorImpl;
import eg.edu.alexu.csd.oop.game.objects.ClownAttributes;
import eg.edu.alexu.csd.oop.game.pool.ShapePool;
public class WorldImpl implements World {	
	//
	public static final int WIDTH = 900;
	public static final int HEIGHT = 680;
	public static final int PIC_DIMENSION = 32;
	public static final int FACTOR = 4;
	//
	protected static final int MAX_TIME = 60 * 1000; // 1 minute
	protected int score = 0;
	protected long startTime = System.currentTimeMillis();
	protected final List<GameObject> constant = new LinkedList<GameObject>();
	protected final List<GameObject> moving = new LinkedList<GameObject>();
	protected final List<GameObject> control = new LinkedList<GameObject>();
	protected ShapePool pool;
	protected int clownsNumber;
	protected String[] randomShapeGenerator = {"BlueShape", "GreenShape", "YellowShape", "RedShape" };
    protected GameObject bar1;
    protected GameObject bar2;
    protected int targetScore;
	protected IteratorImpl iterator;
	public WorldImpl(){
		pool = ShapePool.getInstance();
		//GameObject background = new Background();
		//this.constant.add(background);

	}	
	public void setBarShapes (){
		int shift = 0;
		for (int i = 0; i < 7; i++){		
			GameObject object1 = pool.getShape(randomShapeGenerator[new Random().nextInt(4)]);
			GameObject object2 = pool.getShape(randomShapeGenerator[new Random().nextInt(4)]);
			object1.setX(object1.getX() - shift);
			object2.setX(object2.getX() + shift);
			this.moving.add(object1);
			this.moving.add(object2);
			shift += object1.getWidth();
			
		}
	}
	
	public Map<String,Class<?>> processJar(String path) throws MalformedURLException {
		   Map<String,Class<?>> map = new HashMap<String,Class<?> >() ;
			try {
				File file = new File(path);
				URLClassLoader loader = new URLClassLoader(new URL[]{ file.toURI().toURL() }, ClassLoader.getSystemClassLoader());	
				JarInputStream jis = new JarInputStream(new FileInputStream(path));
					JarEntry je;
					while(true){
						je = jis.getNextJarEntry();
						if(je == null)
							break;
						if(je.getName().endsWith(".class")){
							String x = je.getName().replaceAll("/", ".");
							x = x.split(".class")[0];
							String className = je.getName().substring(0,je.getName().length()-6); 
						    className = className.replace('/', '.');
							try{
								Class<?> tC = loader.loadClass(className);
								////
								 map.put(tC.getSimpleName(),tC) ;
								 System.out.println(tC.getSimpleName());
								////
							}catch(Exception ex) {
								System.out.println("Exception");
							}
						}
					}
				}catch(Exception ex){
					System.out.println("Exception");
				}
			return map;
		}
	
	protected void run(String path) {
	  			Map<String, Class<?>> map = null;
				try {
					map = processJar(path);
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	  			ThemeLoadableClass theme = null ;
				try {
					theme = (ThemeLoadableClass) map.get("Theme").newInstance();
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
	  			theme.setEnvironment(this);
	  			theme.setClasses(map);
	  			pool.setClasses(map);
	  			theme.start();
	  			this.clownsNumber=theme.getClownsNumber();
				map.remove("Theme") ;
				
				
	} 
	

	protected GameObject IntersectBar(GameObject object) {
		Iterator iterator = new IteratorImpl(this.constant);
		iterator.next();
		while (iterator.hasNext()) {
			GameObject bar = iterator.next();
			if (((object.getHeight() + object.getY()) == bar.getY()) && ((isLeft(bar) && object.getX() < bar.getWidth())
					|| (isRight(bar) && object.getX() > bar.getX()-20))) {
				return bar;
			}
		}
		return null;
	}
	
	protected ClownAttributes intersectClown(GameObject object) {
		for (int index = 0; index < this.clownsNumber;index++){
			ClownAttributes clown = (ClownAttributes) this.control.get(index) ;
			if (getClownLeftHand(clown).intersects(getShape(object))) {
				object.setX(clown.getX());
				object.setY(clown.getLeftStackYPosition());
				return clown;
			} else if (getClownRightHand(clown).intersects(getShape(object))) {
				object.setX(clown.getX()+ 3 * (clown.getWidth() / 4)-FACTOR*(FACTOR+1));
				object.setY(clown.getRightStackYPosition());
				return clown;
			}
		}
		return null;
	}
	protected boolean reachBar(GameObject clown){
		ClownAttributes object = (ClownAttributes) clown ;
		return (object.getRightStackYPosition()<=this.bar1.getY()&&object.getLeftStackYPosition()<=this.bar1.getY());
	}

	protected Rectangle getClownLeftHand(ClownAttributes clown){
		return new Rectangle(clown.getX(), clown.getLeftStackYPosition(), clown.getWidth() / FACTOR , 1);
	}

	protected Rectangle getClownRightHand(ClownAttributes clown){
		return new Rectangle(clown.getX() + 3 * (clown.getWidth() / 4), clown.getRightStackYPosition(), clown.getWidth() / FACTOR, 1);
	}
	protected Rectangle getShape(GameObject object) {
		return new Rectangle(object.getX(),object.getY(), object.getWidth(), object.getHeight());
	}

	protected boolean isLeft(GameObject bar) {
		return (bar.getX() == 0);
	}
	protected boolean isRight(GameObject bar) {
		return (bar.getX() != 0);
	}
	@Override
	public List<GameObject> getConstantObjects() {
		// TODO Auto-generated method stub
		return this.constant;
	}
	@Override
	public List<GameObject> getMovableObjects() {
		// TODO Auto-generated method stub
		return this.moving;
	}

	@Override
	public List<GameObject> getControlableObjects() {
		// TODO Auto-generated method stub
		return this.control;
	}

	@Override
	public int getWidth() {
		// TODO Auto-generated method stub
		return WIDTH;
	}
	@Override
	public int getHeight() {
		// TODO Auto-generated method stub
		return HEIGHT;
	}
	@Override
	public boolean refresh() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public String getStatus(){
		// TODO Auto-generated method stub
		return null ;
	}
	@Override
	public int getSpeed(){
		// TODO Auto-generated method stub
		return 20;
	}
	@Override
	public int getControlSpeed() {
		// TODO Auto-generated method stub
		return 10;
	}
}
