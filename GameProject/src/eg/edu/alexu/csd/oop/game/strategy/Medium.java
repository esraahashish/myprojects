package eg.edu.alexu.csd.oop.game.strategy;

public class Medium extends Easy {
	
	public Medium(String path) {
		super(path);
	}
	
	
	@Override
	public int getSpeed() {
		return 15;
	}

	@Override
	public int getControlSpeed() {
		return 25;
	}
}
