package eg.edu.alexu.csd.oop.game.flyweight;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;

public class ImageFactory {

	private HashMap<String,BufferedImage> map = new HashMap<String,BufferedImage>();
	public BufferedImage getShape(String path){
		BufferedImage img = map.get(path);
		if(img == null){
			try {
				img = ImageIO.read(getClass().getResourceAsStream(path));
			} catch (IOException e) {
				e.printStackTrace();
			}
		    map.put(path, img);
		}
	   return img;
	}

}
