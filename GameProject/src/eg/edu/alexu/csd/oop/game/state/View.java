package eg.edu.alexu.csd.oop.game.state;

public class View {

	private State state = null;
	private String className = null;
	private String themePath = null;

	public void setState(State state) {
		this.state = state;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public void setThemePath(String themePath) {
		this.themePath = themePath;
	}
	
	public String getClassName(){
		return className;
	}
	
	public String getThemePath(){
		return themePath;
	}
	
	
	public void start(){
		state.execute();
	}
}
