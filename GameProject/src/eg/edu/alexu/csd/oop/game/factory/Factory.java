package eg.edu.alexu.csd.oop.game.factory;

import java.lang.reflect.InvocationTargetException;

import eg.edu.alexu.csd.oop.game.World;

public class Factory {

	public World getStateObject(String className,String path) {
		try {
			try {
				System.out.println(path);
				return (World)(Class.forName("eg.edu.alexu.csd.oop.game.strategy." + className)).getConstructor(String.class).newInstance(path)  ;
			} catch (IllegalArgumentException | InvocationTargetException | NoSuchMethodException
					| SecurityException e) {
				e.printStackTrace();
				// remove that
				System.out.println("in world factory");
				
				
			}
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
}
