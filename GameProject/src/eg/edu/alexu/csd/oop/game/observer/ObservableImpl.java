package eg.edu.alexu.csd.oop.game.observer;

import java.util.LinkedList;

public class ObservableImpl implements Observable{

	private LinkedList <Observer> observer = new LinkedList<Observer>();
	@Override
	public void notifyAllObservers() {
		for(Observer itr : observer){
			itr.update();
		}
		
	}
	@Override
	public void addObserver(Observer observer) {
		this.observer.add(observer);
		
	}

}
