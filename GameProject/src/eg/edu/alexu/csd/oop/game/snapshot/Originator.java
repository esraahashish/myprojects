package eg.edu.alexu.csd.oop.game.snapshot;

import eg.edu.alexu.csd.oop.game.GameObject;

public class Originator {
	public  Memento save(GameObject gameObject){
		return new Memento(gameObject) ; 
	}

}
