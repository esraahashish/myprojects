package eg.edu.alexu.csd.oop.game.objects;
import eg.edu.alexu.csd.oop.game.GameObject;
import eg.edu.alexu.csd.oop.game.memento.CareTaker;
import eg.edu.alexu.csd.oop.game.memento.Originator;



public interface ClownAttributes extends GameObject{	
	
	public int getLeftStackYPosition() ;
	public int getRightStackYPosition() ;
	public CareTaker getCareTaker() ;
	public Originator getOriginator() ;
	public void setLeftStackYPosition(int leftStackYPosition) ;
	public void setRightStackYPosition(int rightStackYPosition) ;
}
