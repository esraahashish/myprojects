
package eg.edu.alexu.csd.oop.db.xml;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import eg.edu.alexu.csd.oop.db.data.InfoFile;

/**
 * The Class XmlParser.
 */
public class XmlParser {

	/**
	 * Save xml.
	 *
	 * @param data
	 *            the data
	 * @param tableName
	 *            the table name
	 * @param tablePath
	 *            the table path
	 */
	public void SaveXml(Map<String, LinkedList<Object>> data, String tableName, String tablePath) {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(true);
			File table = new File(tablePath.toString() + File.separator + tableName + ".xml");
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.newDocument();
			Element root = document.createElement(tableName);
			Set<String> key = data.keySet();
			Iterator<String> iterator = key.iterator();
			int rowsNumber = 0;
			try {
				String temp = iterator.next();
				rowsNumber = data.get(temp).size();
			} catch (Exception e) {
				rowsNumber = 0;
			}
			InfoFile infoFile = new InfoFile();
			LinkedList<String> columnInfo = infoFile.LoadTableInfo(tablePath + File.separator + tableName + ".xml");
			Attr attribute = document.createAttribute(tableName);
			String typeInfo = "";
			for (int columns = 0; columns < data.size(); columns++) {
				typeInfo += columnInfo.get(columns) + ";";
			}
			attribute.setValue(typeInfo);
			root.setAttributeNode(attribute);
			for (int rowData = 0; rowData < rowsNumber; rowData++) {
				Element row = document.createElement("row");
				for (String columnNames : key) {
					LinkedList<Object> tmp = data.get(columnNames);
					Element column = document.createElement(columnNames);
					column.appendChild(document.createTextNode(String.valueOf(tmp.get(rowData))));
					row.appendChild(column);
				}
				root.appendChild(row);
			}
			document.appendChild(root);
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, tableName + ".dtd");
			DOMSource source = new DOMSource(document);
			StreamResult result = new StreamResult(table);
			transformer.transform(source, result);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return;
	}

	/**
	 * Load xml.
	 *
	 * @param tableName
	 *            the table name
	 * @param tablePath
	 *            the table path
	 * @return the map
	 */
	public Map<String, LinkedList<Object>> loadXml(String tableName, String tablePath) {

		tableName = tableName.toLowerCase();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		Map<String, LinkedList<Object>> Data = new LinkedHashMap<String, LinkedList<Object>>();
		boolean containsData = false;
		try {

			DocumentBuilder builder = factory.newDocumentBuilder();
			File table = new File(tablePath + File.separator + tableName + ".xml");
			Document document = builder.parse(table);
			Node tableTags = document.getDocumentElement();
			NodeList rowTags = tableTags.getChildNodes();
			InfoFile infoFile = new InfoFile();
			LinkedList<String> columnsList = infoFile.LoadTableInfo(tablePath + File.separator + tableName + ".xml");
			for (int data = 0; data < columnsList.size(); data++) {
				Data.put(columnsList.get(data).split(" ")[0], new LinkedList<Object>());
			}
			for (int rowData = 0; rowData < rowTags.getLength(); rowData++) {
				Node row = rowTags.item(rowData);
				NodeList columnTags = row.getChildNodes();
				for (int columnData = 0; columnData < columnTags.getLength(); columnData++) {
					LinkedList<Object> temp = Data.get(columnTags.item(columnData).getNodeName());
					containsData = true;
					if (columnsList.get(columnData).split(" ")[1].equalsIgnoreCase("int")) {
						temp.add(Integer.parseInt(columnTags.item(columnData).getTextContent()));
					} else {
						temp.add(columnTags.item(columnData).getTextContent());
					}
					Data.put(columnTags.item(columnData).getNodeName(), temp);
				}
			}

		} catch (SAXException e) {
			// TODO Auto-generated catch block
			return new LinkedHashMap<String, LinkedList<Object>>();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return new LinkedHashMap<String, LinkedList<Object>>();

		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			return new LinkedHashMap<String, LinkedList<Object>>();
		}
		if (containsData) {
			return Data;

		} else {
			return new LinkedHashMap<String, LinkedList<Object>>();
		}
	}

}
