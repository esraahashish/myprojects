package eg.edu.alexu.csd.oop.db.data;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class InfoFile {
	private String typeInfo = "";

	public String getAttributes() {
		return typeInfo;
	}

	public void saveTableInfo(LinkedList<String> data, String tablePath) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		File table = new File(tablePath + File.separator
				+ data.get(2).toLowerCase() + ".xml");
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			Document document = builder.newDocument();
			Element root = document.createElement(data.get(2).toLowerCase());
			Attr attribute = document
					.createAttribute(data.get(2).toLowerCase());
			for (int columns = 3; columns < data.size(); columns++) {
				typeInfo += data.get(columns) + ";";
			}
			attribute.setValue(typeInfo);
			root.setAttributeNode(attribute);
			document.appendChild(root);
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, data.get(2).toLowerCase() + ".dtd");
			DOMSource source = new DOMSource(document);
			StreamResult result = new StreamResult(table);
			transformer.transform(source, result);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}

	}

	public LinkedList<String> LoadTableInfo(String tablePath) {
		LinkedList<String> columnInfo = new LinkedList<String>();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			File table = new File(tablePath);
			Document document = builder.parse(table);
			Element tableTags = document.getDocumentElement();
			String columnNameType = tableTags.getAttribute(tableTags.getTagName());

			String regex = "(\\w+\\s\\w+);";
			Pattern pattern = Pattern.compile(regex);
			Matcher matcher = pattern.matcher(columnNameType);
			while (matcher.find()) {
				columnInfo.add(matcher.group(1));
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return columnInfo;
	}

}
