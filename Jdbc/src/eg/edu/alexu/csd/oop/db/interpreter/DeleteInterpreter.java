package eg.edu.alexu.csd.oop.db.interpreter;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DeleteInterpreter implements Interpreter{

	@Override
	public LinkedList<String> interpret(String syntax) throws SQLException {
		String regex = "FROM\\s+(\\w+)|WHERE\\s+(\\w+)\\s*(\\W)\\s*(\\'?\\w+\\'?)";
		Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(syntax);
		matcher.find();
		LinkedList<String> list = new LinkedList<String>();
		list.add(matcher.group(1));
		if (matcher.find()) {
			list.add("foundWhere");
			list.add(matcher.group(2));
			list.add(matcher.group(3));
			list.add(matcher.group(4));
		} else {
			list.add("notfoundWhere");
		}
		return list;
	}

}
