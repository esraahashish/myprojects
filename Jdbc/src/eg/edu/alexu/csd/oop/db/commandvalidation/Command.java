package eg.edu.alexu.csd.oop.db.commandvalidation;

public interface Command {

	public boolean validate(String querySyntax, String regex);
}
