package eg.edu.alexu.csd.oop.db.databasefactory;

import java.sql.SQLException;
import java.util.LinkedList;

/**
 * The Class AbstractDirectory.
 */
public abstract class AbstractDirectory {

	/**
	 * Creates the.
	 *
	 * @param query the query
	 * @param path the path
	 * @return true, if successful
	 * @throws SQLException the SQL exception
	 */
	public abstract boolean create(LinkedList<String> query,String path) throws SQLException ;
	
	/**
	 * Drop.
	 *
	 * @param query the query
	 * @param path the path
	 * @return true, if successful
	 * @throws SQLException the SQL exception
	 */
	public abstract boolean drop(LinkedList<String> query,String path) throws SQLException ;
	
}




