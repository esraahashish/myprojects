package eg.edu.alexu.csd.oop.db.commandvalidation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidateImpl implements Command {
	
	public boolean validate(String syntax, String regex) {
		Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(syntax);
		return matcher.find();
	}

}
