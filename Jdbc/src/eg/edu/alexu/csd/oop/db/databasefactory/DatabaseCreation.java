package eg.edu.alexu.csd.oop.db.databasefactory;

import java.io.File;
import java.util.LinkedList;

public class DatabaseCreation extends AbstractDirectory {

	@Override
	public boolean create(LinkedList<String> creationData, String path) {
		File databaseDirectory = new File(path);
		if (!databaseDirectory.mkdirs()) {
			databaseDirectory.mkdirs();
		}
		return true;
	}

	@Override
	public boolean drop(LinkedList<String> dropData, String databasepath) {
		try {
			File databaseDirectory = new File(databasepath);
			String[] tables = databaseDirectory.list();
			if (tables.length != 0) {
				for (String tableName : tables) {
					File currenTable = new File(databasepath, tableName);
					currenTable.delete();
				}
			}
			return true;
		} catch (Exception e) {
			return false;
		}

	}

}
