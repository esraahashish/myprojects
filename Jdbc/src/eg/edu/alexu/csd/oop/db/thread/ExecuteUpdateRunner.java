package eg.edu.alexu.csd.oop.db.thread;

import java.sql.SQLException;

import eg.edu.alexu.csd.oop.db.jdbc.Statement;

public class ExecuteUpdateRunner implements Runnable {
	private String sql = null;
	private Statement statement = null;
	private int updatedRows;
	private int start = 0;
	private int end = 0;
	private int time = 0;

	public ExecuteUpdateRunner(Statement statement) {
		this.statement = statement;
	}

	public int getExecutionTime() {
		return this.time;
	}

	public void setSQL(String sql) {
		this.sql = sql;
	}

	public int getUpdatedRowsCount() {
		return this.updatedRows;
	}

	@Override
	public void run() {
		this.start = (int) System.currentTimeMillis();
		try {
			this.updatedRows = statement.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.end = (int) System.currentTimeMillis();
		this.time = end - start;
	}
}
