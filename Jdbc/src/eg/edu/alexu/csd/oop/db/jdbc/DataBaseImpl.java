package eg.edu.alexu.csd.oop.db.jdbc;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import eg.edu.alexu.csd.oop.db.DataInfo;
import eg.edu.alexu.csd.oop.db.Database;
import eg.edu.alexu.csd.oop.db.commandvalidation.Command;
import eg.edu.alexu.csd.oop.db.commandvalidation.ValidateImpl;
import eg.edu.alexu.csd.oop.db.data.DataInfoImpl;
import eg.edu.alexu.csd.oop.db.databasefactory.AbstractDirectory;
import eg.edu.alexu.csd.oop.db.databasefactory.Factory;
import eg.edu.alexu.csd.oop.db.interpreter.CreateAndDropInterpreter;
import eg.edu.alexu.csd.oop.db.interpreter.Interpreter;
import eg.edu.alexu.csd.oop.db.interpreter.SelectInterpreter;

public class DataBaseImpl implements Database {
	private String path = null;
	private String absolutePath = null;
	private DataInfo dataInfo;
	private Interpreter interpreter;

	public DataBaseImpl(String absolutePath) {
		this.path = null;
		this.absolutePath = absolutePath;
		this.dataInfo = DataInfoImpl.createInstance();
	}

	@Override
	public String createDatabase(String databaseName, boolean dropIfExists) {
		databaseName = databaseName.toLowerCase();
		if (checkExistence(databaseName, this.absolutePath)) {
			if (dropIfExists) {
				try {
					executeStructureQuery("DROP DATABASE " + databaseName);
					executeStructureQuery("CREATE DATABASE " + databaseName);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			return path;
		}

		try {
			executeStructureQuery("CREATE DATABASE " + databaseName);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return path;
	}

	@Override
	public boolean executeStructureQuery(String query) throws SQLException {
		interpreter = new CreateAndDropInterpreter();
		String regex = "\\A\\s*CREATE\\s+DATABASE\\s+\\w+\\s*\\z"
				+ "|\\A\\s*DROP\\s+DATABASE\\s+\\w+\\s*\\z"
				+ "|\\A\\s*DROP\\s+TABLE\\s+\\w+\\s*\\z"
				+ "|\\A\\s*CREATE\\s+TABLE\\s+\\w+\\s*\\(\\s*\\w+\\s+(int|varchar)\\s*(,\\s*\\w+\\s+(int|varchar)\\s*)*\\s*\\)\\s*\\z";
		query = query.toLowerCase();
		Command command = new ValidateImpl();
		if (!command.validate(query, regex)) {
			throw new SQLException();
		}
		LinkedList<String> data = interpreter.interpret(query);
		if (data.get(0).equals("create") && data.get(1).equals("database")) {
			this.path = absolutePath + File.separator + data.get(2);
			this.dataInfo.setDataBasePath(path);
		}
		Factory factory = new Factory();
		AbstractDirectory operation = factory.newInstance(data.get(1));
		try {
			Method method = operation.getClass().getMethod(
					data.get(0).toLowerCase(), LinkedList.class, String.class);
			return (boolean) method.invoke(operation, data, path);
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException
				| SecurityException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public Object[][] executeQuery(String query) throws SQLException {
		interpreter = new SelectInterpreter();
		String regex = "\\A\\s*SELECT\\s+\\w+\\s*(\\,\\s*\\w+\\s*)*\\s*FROM\\s+\\w+\\s*\\z"
				+ "|\\A\\s*SELECT\\s*\\*\\s*FROM\\s+\\w+\\s+WHERE\\s+\\w+\\s*(<|>|=)\\s*\\'?\\w+\\'?\\s*\\z"
				+ "|\\A\\s*SELECT\\s*\\*\\s*FROM\\s+\\w+\\s*\\z"
				+ "|\\A\\s*SELECT\\s+\\w+\\s*(\\,\\s*\\w+\\s*)*\\s*FROM\\s+\\w+\\s+WHERE\\s+\\w+\\s*(<|>|=)\\s*\\'?\\w+\\'?\\s*\\z";
		query = query.toLowerCase();
		Command command = new ValidateImpl();
		if (!command.validate(query, regex)) {
			throw new SQLException();
		}
		LinkedList<String> data = interpreter.interpret(query);
		String queryType = actionType(query);
		Factory factory = new Factory();
		AbstractDirectory table = factory.newInstance("table");

		try {
			Method method = table.getClass().getMethod(queryType.toLowerCase(), LinkedList.class, String.class);
			return (Object[][]) method.invoke(table, data, path);
		} catch (NoSuchMethodException | SecurityException
				| IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
		}
		return new Object[0][0];
	}

	@Override
	public int executeUpdateQuery(String query) throws SQLException {
		String regex = "\\A\\s*INSERT\\s+INTO\\s+\\w+\\s*(\\(\\s*\\w+\\s*(,\\s*\\w+\\s*)*\\s*\\)\\s*)?VALUES\\s*\\(\\s*\\'?\\w+\\'?\\s*(,\\s*\\'?\\w+\\'?\\s*)*\\s*\\)\\s*\\z"
				+ "|\\A\\s*UPDATE\\s+\\w+\\s+SET\\s+\\w+\\s*=\\s*\\'?\\w+\\'?\\s*(,\\s*\\w+\\s*=\\s*\\'?\\w+\\'?\\s*)*(WHERE\\s+\\w+\\s*(=|>|<)\\s*\\'?\\w+\\'?)?\\s*\\Z"
				+ "|\\ADELETE\\s+FROM\\s+\\w+(\\s+WHERE\\s+\\w+\\s*(=|>|<)\\s*\\'?\\w+\\'?)?\\s*\\z"
				+ "|\\ADELETE\\s+\\*\\s+FROM\\s+\\w+\\s*\\z";
		query = query.toLowerCase();
		Command command = new ValidateImpl();
		if (!command.validate(query, regex)) {
			throw new SQLException();
		}

		Factory factory = new Factory();
		AbstractDirectory table = factory.newInstance("table");
		String action = actionType(query).toLowerCase();
		String actionClass = action.substring(0, 1).toUpperCase()+ action.substring(1) + "Interpreter";
		Method method;
		try {
			interpreter = (Interpreter) Class.forName(
					"eg.edu.alexu.csd.oop.db.interpreter." + actionClass)
					.newInstance();
			LinkedList<String> data = interpreter.interpret(query);
			if (!checkExistence(data.get(0) + ".xml", path)) {
				throw new SQLException();
			}
			method = table.getClass().getMethod(action.toLowerCase(),
					LinkedList.class, String.class);
			return (int) method.invoke(table, data, path);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		}

		return 0;
	}

	public boolean checkExistence(String name, String path) {
		name = name.toLowerCase();
		try {
			File directory = new File(path);
			String[] files = directory.list();
			for (String file : files) {
				if (file.toLowerCase().equals(name)) {
					return true;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	private String actionType(String query) {
		String columnRegex = "\\A\\s*(SELECT|INSERT|DELETE|UPDATE)";
		Pattern pattern = Pattern
				.compile(columnRegex, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(query);
		matcher.find();
		return matcher.group(1);
	}

}
