
package eg.edu.alexu.csd.oop.db.xml;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;

/**
 * The Class DtdParser.
 */
public class DTD {

	/**
	 * Save dtd file.
	 *
	 * @param tableData the table data
	 * @param tablePath the table path
	 */
	public void SaveDtdFile(LinkedList<String> tableData, String tablePath , String attr) {
		File dtdFile = new File(tablePath.toString() + File.separator + tableData.get(2).toLowerCase() + ".dtd");
		try {
			BufferedWriter fileWriter = new BufferedWriter(new FileWriter(dtdFile));
			fileWriter.write("<!ATTLIST " + tableData.get(2) + " " + tableData.get(2) + " CDATA " + "\"" +  attr + "\">\n");
			fileWriter.write("<!ELEMENT " + tableData.get(2) + " (row)*>\n");
			fileWriter.write("<!ELEMENT Row (");
			for (int data = 3; data < tableData.size(); data++) {
				if (data != tableData.size() - 1)
					fileWriter.write(tableData.get(data).split(" ")[0]+ ", ");
				else
					fileWriter.write(tableData.get(data).split(" ")[0]);
				
			}
			fileWriter.write(")>\n");
			for (int data = 3; data < tableData.size(); data++) {
				fileWriter.write("<!ELEMENT " + tableData.get(data).split(" ")[0] + " (#PCDATA)>\n");
			}
			fileWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
