package eg.edu.alexu.csd.oop.db.interpreter;

import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CreateAndDropInterpreter implements Interpreter {

	@Override
	public LinkedList<String> interpret(String syntax) {
		String regex = "(CREATE|DROP)\\s+(DATABASE|TABLE)\\s+(\\w+)|(\\w+)\\s+(int|varchar)";
		Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(syntax);
		LinkedList<String> list = new LinkedList<String>();
		matcher.find();
		list.add(matcher.group(1));
		list.add(matcher.group(2));
		list.add(matcher.group(3));
		while (matcher.find()) {
			list.add(matcher.group(4)+" "+matcher.group(5));
		}
		return list;
	}

}
