package eg.edu.alexu.csd.oop.db.data;

import java.io.File;
import java.util.LinkedList;

import eg.edu.alexu.csd.oop.db.DataInfo;

/**
 * The Class DataInfoImpl.
 */
public class DataInfoImpl implements DataInfo {

	/** The table path. */
	private LinkedList<String> selectedTable = new LinkedList<String>();

	private String dataBasePath = null;

	private static DataInfoImpl dataInfoImpl = new DataInfoImpl();

	private DataInfoImpl() {
	}

	public static DataInfoImpl createInstance() {
		return dataInfoImpl;
	}

	@Override
	public LinkedList<String> getColumnType() {

		InfoFile infoFile = new InfoFile();
		LinkedList<String> fileInfo = new LinkedList<String>();
		fileInfo = infoFile.LoadTableInfo(this.dataBasePath + File.separator
				+ selectedTable.get(0) + ".xml");
		LinkedList<String> columnType = new LinkedList<String>();

		if (selectedTable.contains("all")) {
			for (int columnIndex = 0; columnIndex < fileInfo.size(); columnIndex++) {
				columnType.add(fileInfo.get(columnIndex).split(" ")[1]);
			}
		} else {
			for (int columnIndex = selectedTable.indexOf("specific") + 1; columnIndex < selectedTable
					.size(); columnIndex++) {
				String columnName = selectedTable.get(columnIndex);
				for (int data = 0; data < fileInfo.size(); data++) {
					if (fileInfo.get(data).split(" ")[0]
							.equalsIgnoreCase(columnName)) {
						columnType.add(fileInfo.get(data).split(" ")[1]);
					}
				}
			}
		}
		return columnType;
	}

	@Override
	public LinkedList<String> getColumnName() {
		InfoFile infoFile = new InfoFile();
		LinkedList<String> fileInfo = new LinkedList<String>();
		fileInfo = infoFile.LoadTableInfo(this.dataBasePath + File.separator
				+ selectedTable.get(0) + ".xml");
		LinkedList<String> columnName = new LinkedList<String>();

		if (selectedTable.contains("all")) {
			for (int columnIndex = 0; columnIndex < fileInfo.size(); columnIndex++) {
				columnName.add(fileInfo.get(columnIndex).split(" ")[0]);
			}
		} else {
			int columnIndex = selectedTable.indexOf("specific") + 1;
			for (; columnIndex < selectedTable.size(); columnIndex++) {
				columnName.add(selectedTable.get(columnIndex));

			}
		}
		return columnName;
	}

	@Override
	public void setSelectedTable(LinkedList<String> selectedTable) {
		this.selectedTable = selectedTable;
	}

	@Override
	public void setDataBasePath(String dataBasePath) {
		this.dataBasePath = dataBasePath;
	}

}